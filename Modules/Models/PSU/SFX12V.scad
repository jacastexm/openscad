 /**
 * Módulo para generar los espacios para colocar fuentes SFX12V en los diseños.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Models/PSU/SFX12V.scad
 * @license CC-BY-NC-4.0
 */
//-----------------------------------------------------------------------------
use <../../builder.scad>
//-----------------------------------------------------------------------------
/**
 * Devuelve las dimensiones de la fuente.
 *
 * @param {Float} tolerance Tolerancia a usar en los valores.
 * @param {Float} screw     Diámetro a usar para los tornillos.
 */
function sfx12v(tolerance = 0, screw = 3.5) = let (
    _x  = 125.0 + tolerance,
    _y  =  63.5 + tolerance,
    _z  = 100.0 + tolerance,
    _dx = 113.0 / 2,
    _dy =  51.5 / 2,
    _dz = _z / 2
) [
    _x, // Ancho (eje X)
    _y, // Alto (eje Y),
    _z, // Largo (eje Z)
    [
        // Tornillos para sujertar la fuente al chasis.
        [ "circle", [ screw ], [ - _dx, - _dy, - _dz ], [ 180, 0, 0 ] ],
        [ "circle", [ screw ], [ - _dx,   _dy, - _dz ], [ 180, 0, 0 ] ],
        [ "circle", [ screw ], [   _dx, - _dy, - _dz ], [ 180, 0, 0 ] ],
        [ "circle", [ screw ], [   _dx,   _dy, - _dz ], [ 180, 0, 0 ] ]
    ],
    [
        // TooQ TQEP-500S-SFX
        [
            [ "square", [ 48.0, 48.0 ], [ - 29.5,     0.00, -  _dz ], [ 180, 0, 0 ] ], // Extractor
            [ "square", [ 31.8, 25.0 ], [   31.1,     1.10, -  _dz ], [ 180, 0, 0 ] ], // Enchufe
            [ "square", [ 15.5, 21.7 ], [    4.9,     1.25, -  _dz ], [ 180, 0, 0 ] ], // Interruptor
            [ "circle", [ screw      ], [ - 49.5, - _y / 2,   44.5 ], [  90, 0, 0 ] ], // Agujero de la tapa.
            [ "circle", [ screw      ], [ - 49.5, - _y / 2, - 44.5 ], [  90, 0, 0 ] ], // Agujero de la tapa.
            [ "circle", [ screw      ], [   49.5, - _y / 2,   44.5 ], [  90, 0, 0 ] ], // Agujero de la tapa.
            [ "circle", [ screw      ], [   49.5, - _y / 2, - 44.5 ], [  90, 0, 0 ] ], // Agujero de la tapa.
        ]
    ]
];

/**
 * Genera un modelo que puede ser usado como negativo para marcar los tornillos y
 * rectángulos de la fuente.
 *
 * @param {Float}   thickness Grosor de las paredes donde se imprimirá el modelo.
 * @param {Integer} modelo    Modelo de fuente SFX.
 * @param {Float}   tolerance Tolerancia a usar en las medidas.
 */
module sfx12vModel(thickness = 5, model = 0, tolerance = 0.9)
{
    _SFX = sfx12v(tolerance);
    builder(
        concat(
            [ [ "cube", [ _SFX[0], _SFX[1], _SFX[2] ] ] ],
            _SFX[3],
            _SFX[4][model]
        ),
        thickness
    );
}
