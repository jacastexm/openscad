 /**
 * Módulo para generar los espacios para colocar fuentes ATX12V en los diseños.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Models/PSU/ATX12V.scad
 * @license CC-BY-NC-4.0
 */
//----------------------------------------------------------
use <../../builder.scad>
//----------------------------------------------------------
/**
 * Devuelve las dimensiones de la fuente.
 *
 * @param {Float} tolerance Tolerancia a usar en los valores.
 * @param {Float} screw     Diámetro a usar para los tornillos.
 */
function atx12v(tolerance = 0, screw = 3.5) = let (
    _x  = 150.0 + tolerance,
    _y  =  86.0 + tolerance,
    _z  = 140.0 + tolerance,
    _dx = 113.0 / 2,
    _dy =  51.5 / 2,
    _dz = _z / 2
) [
    _x, // Ancho (eje X)
    _y, // Alto (eje Y),
    _z, // Largo (eje Z)
    [
        // Tornillos para sujertar la fuente al chasis.
        [ "circle", [ screw  ], [ - 69,   27, - _dz ], [ 180, 0, 0 ] ],
        [ "circle", [ screw  ], [ - 69, - 37, - _dz ], [ 180, 0, 0 ] ],
        [ "circle", [ screw  ], [   45,   37, - _dz ], [ 180, 0, 0 ] ],
        [ "circle", [ screw  ], [   69, - 37, - _dz ], [ 180, 0, 0 ] ],
        [ "square", [ 75, 65 ], [ - 28,    0, - _dz ], [ 180, 0, 0 ] ] // Extractor
    ],
    [
        // Tacens Anima APII 500
        [
            [ "square", [ 51.8, 23.5 ], [ 44.9,  2.3, - _dz ], [ 180, 0, 0 ] ],
            [ "square", [ 22.0, 15.1 ], [ 44.6, 21.5, - _dz ], [ 180, 0, 0 ] ]
        ]
    ]
];

/**
 * Genera un modelo que puede ser usado como negativo para marcar los tornillos y
 * rectángulos de la fuente.
 *
 * @param {Float}   thickness Grosor de las paredes donde se imprimirá el modelo.
 * @param {Integer} modelo    Identificador del modelo de fuente a usar.
 * @param {Float}   tolerance Tolerancia a usar en las medidas.
 */
module atx12vModel(thickness = 5, model = 0, tolerance = 0.9)
{
    _ATX = atx12v(tolerance);
    builder(
        concat(
            [ [ "cube", [ _ATX[0], _ATX[1], _ATX[2] ] ] ],
            _ATX[3],
            _ATX[4][model]
        ),
        thickness
    );
}
