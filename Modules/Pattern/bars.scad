/**
 * Utilidades para trabajar con patrones de barras.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Pattern/bars.scad
 * @license CC-BY-NC-4.0
 */
//-----------------------------------------------------------------------------
use <../Utils/flex.scad>
//-----------------------------------------------------------------------------
/**
 * Dibuja una barra que puede tener los extremos de diversas formas.
 *
 * @param {Float}  width  Ancho de cada barra (eje X).
 * @param {Float}  height Alto de cada barra (eje Y).
 * @param {String} type   Tipo de extermos a generar:
 *                        - triangle : Tríángulos de 45º para que puedan ser impresos.
 *                        - rtriangle: Tríángulos de 45º pero con las puntas redondeadas..
 *                        - rounded  : Redodeado. El diámetro es el ancho de la barra.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Pattern/bars.scad
 * @license CC-BY-NC-4.0
 */
module bar2d(width, height, type = "triangle")
{
    if (type == "triangle")
    {
        _h = height / 2;
        _w = width / 2;
        _a = _h - _w;
        polygon(
            points = [
                [    0, - _h ],
                [   _w, - _a ],
                [   _w,   _a ],
                [    0,   _h ],
                [ - _w,   _a ],
                [ - _w, - _a ],
            ]
        );
    }
    else if (type == "rtriangle")
    {
        _d = 1.5;
        _h = (height - _d) / 2;
        _w = (width - _d) / 2;
        hull()
        {
            for (_p = [ [ - _w, _h - _w - _d / 2 ], [ 0, _h ], [ _w, _h - _w - _d / 2 ] ])
            {
                for (_y = [ -1, 1 ])
                {
                    translate([ _p[0], _y * _p[1] ])
                    {
                        circle(d = _d);
                    }
                }
            }
        }
    }
    else if (type == "rounded")
    {
        for (_y = [ -1, 1 ])
        {
            translate([ 0, _y * (height - width) / 2 ])
            {
                circle(d = width);
            }
        }
        square([ width, height - width ], center = true);
    }
    else
    {
        square([ width, height ], center = true);
    }
}

/**
 * Dibuja unas barras de manera repetida hasta completar la longitud.
 *
 * @param {Float}     length Longitud del área a dibujar (eje X).
 * @param {Float}     width  Ancho de cada barra (eje X).
 * @param {Float}     height Alto de cada barra (eje Y).
 * @param {Float}     count  Cantidad de barras a dibujar.
 * @param {String}    type   Tipo de barra a generar.
 * @param {Integer[]} skip   Índice de las barras a omitir.
 */
module bars(length, width, height, count = 10, type = "rtriangle", skip = [])
{
    flexCenter(length, floor(count), skip = skip)
    {
        bar2d(width, height, type);
    }
}

/**
 * Dibuja unas barras y las rota 90º al centrarlas.
 *
 * Su principal uso es llenar un área con un grupo de barras dejando espacio entre ellas.
 *
 * Por ejemplo, en vez de hacer unas barras de 100 de largo, se pueden separar en grupos
 * de 30 dejando 10 para unir los grupos fortaleciendo las barras.
 *
 * Al módulo recibe como hijo las barras.
 *
 * @param {Float}   length Longitud a llenar con las barras.
 * @param {Integer} count  Cantidad de veces que se repetirá el grupo de barras.
 */
module fbars(length, count = 5)
{
    _l = length / count;
    flexCenter(length - _l, count)
    {
        rotate(90)
        {
            children();
        }
    }
}
