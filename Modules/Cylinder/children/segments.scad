/**
 * Utilidad para generar un cilindro dividido en segmentos radiales.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Cylinder/children/segments.scad
 * @license CC-BY-NC-4.0
 */
//---------------------------------------------------------------
use <../../Shapes/propeller.scad>
use <../../../Functions/Utils/stlName.scad>
//---------------------------------------------------------------
/**
 * Divide un cilindro en segmentos radiales.
 *
 * @param {Float} diameter  Diámetro del cilindro.
 * @param {Float} length    Longitud del cilindro (eje Z).
 * @param {Float} thickness Grosor de las paredes del segmento.
 * @param {Float} count     Cantidad de segmentos.
 * @param {String} name     Nombre del modelo a generar.
 */
module cylinderSegments(diameter, length, thickness, count = 3, name = "cylinder-segments")
{
    if ($children)
    {
        echo(stlName(str(name, "-", "custom"), [ diameter, length, thickness, count ]));
        difference()
        {
            children();
            propeller(diameter, length, thickness, [ 0 : 360 / count : 359 ]);
        }
    }
    else
    {
        echo(stlName(name, [ diameter, length, thickness, count ]));
        propeller(diameter, length, thickness, [ 0 : 360 / count : 359 ]);
    }
}
