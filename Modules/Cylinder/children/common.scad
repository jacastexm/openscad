/**
 * Módulos y funciones que permiten acomodar los hijos dentro de un cilindro.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Cylinder/children/common.scad
 * @license CC-BY-NC-4.0
 */
//---------------------------------------------------------------
use <../../../Functions/Math/line.scad>
use <../../../Functions/Utils/stlName.scad>
use <../../Box/rounded.scad>
use <../../Pattern/concentric.scad>
use <../../Pattern/rectangular.scad>
use <../arc.scad>
//---------------------------------------------------------------
/**
 * Obtiene el valor máximo en Y a partir de X.
 *
 * @param {Float} d Diámetro interior del cilindro.
 * @param {Float} x Valor de la coordenada X.
 *
 * @return {Float} Valor máximo de Y.
 */
function cylinderCalc(d, x) = x <= d / 2
    ? sqrt(pow(d / 2, 2) - pow(x, 2)) * 2
    : -1;

/**
 * Genera un grupo de cilindros concéntricos.
 *
 * @param {Float} diameter  Diámetro del cilindro..
 * @param {Float} length    Longitud del cilindro (eje Z).
 * @param {Float} thickness Grosor de las paredes de cada cilindro.
 * @param {Float} count     Cantidad de cilindros.
 * @param {Float} sides     Cantidad de lados de cada arco.
 */
module cylinderArcsConcentric(diameter, length, thickness = 1.8, count = 2, sides = 360, name = "concentric-arcs")
{
    _r  = diameter / 2;
    _dr = (_r + thickness) / (count + 1);
    cylinderFrom2dChildren(diameter, length, thickness, stlName(name, [ diameter, length, thickness, count, sides ]))
    {
        difference()
        {
            circle(r = _r + 0.1);
            arcs(_dr, _r, thickness, $fn = sides);
        }
    }
}

/**
 * Genera un cilindro y extrae el patrón 2D pasado como hijo del módulo.
 *
 * @param {Float}  diameter Diámetro del cilindro.
 * @param {Float}  length   Longitud a extruir el patrón 2D (eje Z).
 * @param {String} name     Nombre del modelo a generar.
 */
module cylinderFrom2dChildren(diameter, length, thickness, name = "")
{
    if (name)
    {
        echo(
            is_undef(STL_PREFIX)
                ? name
                : str(STL_PREFIX, name)
        );
    }
    difference()
    {
        cylinder(d = diameter, h = length);
        translate([ 0, 0, - 0.001 ])
        {
            linear_extrude(length + 0.002, convexity = 10)
            {
                children();
            }
        }
    }
}

/**
 * Repite un forma 2D generando un patrón concéntrico centrado.
 *
 * @param {Float}  diameter  Diámetro del cilindro (eje radial).
 * @param {Float}  length    Longitud del cilindro (eje Z).
 * @param {Float}  thickness Grosor de las paredes del patrón.
 * @param {Float}  width     Ancho de cada elemento (eje circular).
 * @param {Float}  height    Altura de cada elemento (eje radial).
 * @param {Float}  from      Radio inicial para empezar (eje radial).
 * @param {String} name      Nombre del modelo a generar.
 */
module cylinderShapeConcentric(diameter, length, thickness = 0.6, width = 10, height = 10, from = 0, name = "cylinder-concentric")
{
    cylinderFrom2dChildren(diameter, length, thickness, stlName(name, [ diameter, length, thickness, width, height, from ]))
    {
        concentric(diameter, width, height, thickness, from)
        {
            if ($children)
            {
                children();
            }
            else
            {
                circle(d = min(width, height));
            }
        }
   }
}

/**
 * Repite un forma 2D generando un patrón rectangular centrado.
 *
 * @param {Float} diameter  Diámetro del cilindro.
 * @param {Float} length    Longitud del cilindro (eje Z).
 * @param {Float} thickness Grosor de las paredes del patrón.
 * @param {Float} width     Ancho de cada elemento (eje X).
 * @param {Float} height    Altura de cada elemento (eje Y).
 * @param {String} name     Nombre del modelo a generar.
 */
module cylinderShapeRectangular(diameter, length, thickness = 1.8, width = 10, height = 10, name = "cylinder-rectangular")
{
    cylinderFrom2dChildren(diameter, length, thickness, stlName(name, [ diameter, length, thickness, width, height ]))
    {
        rectangular(diameter, width, height, thickness)
        {
            if ($children)
            {
                children();
            }
            else
            {
                square([ width, height ]);
            }
        }
    }
}

/**
 * Repite la figura 2D sobre el eje Y para generar el patrón 2D a extruir.
 * Se suele usar junto a la función `calc`.
 *
 * @param {Float} length    Longitud del segmento (eje Y).
 * @param {Float} thickness Grosor de las paredes a dejar entre las figuras.
 * @param {Float} width     Ancho que ocupa la figura 2D.
 * @param {Float} height    Alto que ocupa la figura 2D.
 */
module cylinderShapeRepeatY(length, thickness, width, height)
{
    _count = floor((length + thickness) / (height + thickness));
    _total = _count * (height + thickness) - thickness;
    for (_n = [ 0 : _count - 1 ])
    {
        translate([ 0, _n * (height + thickness) - (_total - height) / 2 ])
        {
            children();
        }
    }
}

/**
 * Genera el texto a imprimir en el cilindro.
 *
 * @param {Float}  diameter  Diámetro externo del cilindro.
 * @param {Float}  z         Coordenada Z de la posición del texto.
 * @param {String} text      Texto a imprimir.
 * @param {Float}  size      Tamaño de la fuente.
 * @param {Float}  thickness Grosor del texto.
 * @param {String} valign    Alineación vertical de las letras.
 * @param {String} font      Nombre de la fuente a usar.
 */
module cylinderText(diameter, z, text, thickness = 1, size = 8, valign = "bottom", font = "Arial")
{
    _len    = len(text);
    _rad    = PI / 180;
    _radius = diameter / 2;
    _step   = size / _radius;
    rotate([ 0, 0, - 0.5 * _step * (_len - 1) / _rad ])
    {
        for(_i = [ 0 : _len ])
        {
            rotate(180 + _i * _step / _rad)
            {
                translate([0, _radius - thickness, z - size ])
                {
                    rotate([ 90, 0, 180 ])
                    {
                        linear_extrude(thickness, convexity = 10)
                        {
                            text(
                                text[_i],
                                font   = font,
                                size   = size,
                                halign = "center",
                                valign = valign
                            );
                        }
                    }
                }
            }
        }
    }
}
