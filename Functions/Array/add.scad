/**
 * Suma o resta a todos los elementos del array el valor especificado.
 * Su principal uso es agregar un valor de tolerancia a un array de valores.
 *
 * Ejemplo:
 *
 * in = [ 1, 2, 3, 4 ];
 * echo(arrayAdd(in,  0.25)); // [ 1.25, 2.25, 3.25, 4.25 ]
 * echo(arrayAdd(in, -0.25)); // [ 0.75, 1.75, 2.75, 3.75 ]
 *
 * @param {Float[]} array Array a modificar.
 * @param {Floar}   value Valor a sumar/restar a cada elemento del array.
 *
 * @author  Joaquín Fernández
 * @license CC-BY-NC-4.0
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Functions/Array/add.scad
 */
function arrayAdd(array, value) = [ for (_item = array) _item + value ];
