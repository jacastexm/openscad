/**
 * Genera un contenedor para puntas de impresión.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Storage/Pringles/nozzles.scad
 * @license CC-BY-NC-4.0
 * @see     https://www.thingiverse.com/thing:3615660
 */
//---------------------------------------------------------------
use     <../../Functions/Utils/stlName.scad>
use     <../../Modules/Cylinder/children/common.scad>
include <./container.scad>
//---------------------------------------------------------------
/**
 * Dibuja la llave allen para dejar el espacio mediante diferencia.
 *
 * @param {Float} width    Ancho de la llave (eje X).
 * @param {Float} height   Alto de la llave (eje Y).
 * @param {Float} diameter Diámetro de la llave.
 */
module allen(width = 20, height = 50, diameter = 2)
{
    _delta = diameter * 3;
    translate([ width / 2, - _delta / 4 ])
    {
        rotate([ 90, 0, 0 ])
        {
            cylinder(d = diameter, h = height - _delta / 2 + 0.001, center = true);
        }
    }
    translate([ - _delta / 4, height / 2 ])
    {
        rotate([ 0, 90, 0 ])
        {
            cylinder(d = diameter, h = width - _delta / 2 + 0.001, center = true);
        }
    }
    translate([ width / 2 - _delta / 2, height / 2 - _delta / 2 ])
    {
        rotate_extrude(angle = 90, convexity = 10)
        {
            translate([ _delta / 2, 0 ])
            {
                circle(d = diameter);
            }
        }
    }
}

/**
 * Dibuja el contenedor para almacenar solamente puntas de impresión.
 *
 * @param {Float}  length   Altura total de la punta (eje Z).
 * @param {Float}  plength  Altura de la rosca de la punta (eje Z).
 * @param {Float}  diameter Diámetro de la punta.
 * @param {String} type     Tipo de patrón a usar ("conc" o "rect").
 */
module nozzles(length = 14, plength = 6, diameter = 6, type = "conc")
{
    if (type == "conc")
    {
        conc(length, plength, 6, 6, 1.5)
        {
            circle(d = diameter);
        }
    }
    else if (type == "rect")
    {
        rect(length, plength, 6, 6, 1.5)
        {
            circle(d = diameter);
        }
    }
}

/**
 * Dibuja el contenedor para puntas de impresión, bloque del extrusor y llaves allen.
 *
 * @param {Float} length   Longitud del contenedor (eje Z).
 * @param {Float} plength  Longitud del soporte (eje Z).
 * @param {Float} diameter Diámetro de la punta.
 */
module nozzlesAll(length = 31, plength = 12, diameter = 6)
{
    _allen = 2.5;
    _block = [ 20.6, 10.6 ];
    container(length)
    {
        difference()
        {
            _d = INNER + 0.01;
            cylinderFrom2dChildren(_d, plength, THICKNESS, stlName("Pringles-nozzlesAll", [ length, plength, diameter ]))
            {
                cylinderShapeRepeatY(cylinderCalc(_d, _block[0] / 2) - 2 * _allen, 1.5, _block[0], _block[1])
                {
                    square(_block, center = true);
                }
                // Puntas estrechas
                for (_x = [ -16.7, - 23.7, -30.7 ])
                {
                    translate([ _x, 0 ])
                    {
                        cylinderShapeRepeatY(cylinderCalc(_d, _x - diameter / 2), 0.3, 7.5, 7.5)
                        {
                            circle(d = diameter);
                        }
                    }
                }
                // Puntas anchas
                for (_x = [ 18, 27.5 ])
                {
                    translate([ _x, 0 ])
                    {
                        cylinderShapeRepeatY(cylinderCalc(_d, _x), 0, 10, 10)
                        {
                            circle(d = diameter);
                        }
                    }
                }
            }
            for (_s = [ -1, 1 ])
            {
                translate([ _s * 2, _s * 8, plength ])
                {
                    rotate([ 0, 0, 90 - _s * 90 ])
                    {
                        allen(diameter = _allen);
                    }
                }
            }
        }
    }
}
//-----------------------------------------------------------------------------
$fn = $preview ? 30 : 180;
difference()
{
    nozzlesAll();
    cylinderText(diameter = OUTER, z = LENGTH / 2, text = "NOZZLES", size = 6);
}
