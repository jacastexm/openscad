/**
 * Módulo para generar modelos de tornillos para realizar las diferencias.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/screw.scad
 * @license CC-BY-NC-4.0
 */
//-----------------------------------------------------------------------------
module screwCheckDiameters(screwDiameter, headDiameter)
{
    assert(
        screwDiameter <= headDiameter,
        "El diámetro de la cabeza del tornillo no puede ser menor que el diámetro del tornillo"
    );
}
//-----------------------------------------------------------------------------
/**
 * Dibuja un tornillo con cabeza cilíndrica.
 *
 * @param {Float}   height   Altura del tornillo incluyendo la cabeza.
 * @param {Float}   diameter Diámetro del tornillo.
 * @param {Float[]} head     Dimensiones de la cabeza ([ diámetro, altura ]).
 */
module screwCylHead(height, diameter, head = [])
{
    cylinder(d = diameter, h = height, center = true);
    if (head)
    {
        screwCheckDiameters(diameter, head[0]);
        translate([ 0, 0, (height - head[1]) / 2 ])
        {
            cylinder(d = head[0], h = head[1], center = true);
        }
    }
}
/**
 * Dibuja un tornillo con cabeza plana.
 *
 * @param {Float}   height   Altura del tornillo incluyendo la cabeza.
 * @param {Float}   diameter Diámetro del tornillo.
 * @param {Float[]} head     Dimensiones de la cabeza ([ diámetro, ángulo ]).
 */
module screwFlatHead(height, diameter, head = [])
{
    cylinder(d = diameter, h = height, center = true);
    if (head)
    {
        screwCheckDiameters(diameter, head[0]);
        _r = head[0] / 2;       // Radio de la cabeza.
        _a = head[1] / 2;       // Ángulo medio.
        _h = _r * tan(90 - _a); // Altura de la cabeza
        translate([ 0, 0, height / 2 - _h ])
        {
            rotate_extrude(convexity = 10)
            {
                polygon(
                    points = [
                        [  0, 0  ],
                        [  0, _h ],
                        [ _r, _h ]
                    ]
                );
            }
        }
        translate([ 0, 0, height / 2 ])
        {
            cylinder(r = _r, h = 0.1);
        }
    }
}
/**
 * Dibuja los agujeros de los tonillos.
 *
 * @param {Float}   dx       Separación entre tornillos (eje X).
 * @param {Float}   dy       Separación entre tornillos (eje Y).
 * @param {Float}   height   Altura del tornillo incluyendo la cabeza.
 * @param {Float}   diameter Diámetro del tornillo.
 * @param {Float[]} head     Dimensiones de la cabeza ([ diámetro, ángulo ]).
 * @param {String}  type     Tipo de tornillo.
 */
module screwHoles(dx, dy, height, diameter, head = [], type = "")
{
    for (_x = [ -1, 1 ])
    {
        for (_y = [ -1, 1 ])
        {
            translate([ _x * dx / 2, _y * dy / 2, 0 ])
            {
                if (type == "cyl")
                {
                    screwCylHead(height, diameter, head);
                }
                else if (type == "flat")
                {
                    screwFlatHead(height, diameter, head);
                }
                else if ($children)
                {
                    children();
                }
                else
                {
                    cylinder(d = diameter, h = height, center = true);
                }
            }
        }
    }
}
