/**
 * Utilidades para dibujar soportes para puntas de taladro hexagonales.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Pattern/bits.scad
 * @license CC-BY-NC-4.0
 */
//-----------------------------------------------------------------------------
use <../Box/rounded.scad>
use <../Pattern/rectangular.scad>
//-----------------------------------------------------------------------------

INCH = 25.4;

/**
 * Dibuja todos los soportes para las puntas de taladro que quepan en el área especificada.
 *
 * @param {Float} width     Ancho utilizable.
 * @param {Float} height    Alto utilizable
 * @param {Float} size      Tamaño de la punta.
 * @param {Float} margin    Separación entre filas.
 * @param {Float} tolerance Tolerancia a usar para dejar holgura entre la punta y el agujero.
 */
module bits(width = 20, height = 20, size = INCH * (1/4), margin = 5, tolerance = 0.5)
{
    _d = size / cos(30) + tolerance;
    rectangular(width, _d, size, margin, height)
    {
        circle(d = _d, $fn = 6);
    }
}

/**
 * Dibuja una caja con bordes redondeados donde se puede almacenar las puntas de taladro.
 *
 * @param {Float} width  Ancho de la caja (eje X).
 * @param {Float} height Largo de la caja /eje Y).
 * @param {Float} length Altura de la caja (eje Z).
 * @param {Float} size   Tamaño de la punta.
 * @param {Float} margin Separación entre filas.
 * @param {Float} radius Radio para redondear las esquinas.
 * @param {Float} offset Valor a desplazar la punta en el eje Z para dejar una base.
 */
module bitsBox(width, height, length, size = INCH * (1/4), margin = 2, radius = 1, offset = 1)
{
    difference()
    {
        boxRounded(width, height, length, radius);
        translate([ 0, 0, offset - length / 2 ])
        {
            linear_extrude(length, convexity = 10)
            {
                bits(width - size, height - size, size, margin);
            }
        }
    }
}
