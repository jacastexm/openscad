/**
 * Genera un tornillo con su respectiva tuerca para sujetar carretes de
 * cualquier tipo y tamaño, principalmente pensado para rollos de filamento.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/3DPrinter/SpoolHolder.scad
 * @license CC-BY-NC-4.0
 * @see     https://www.thingiverse.com/thing:3838390
 */
//-----------------------------------------------------------------------------
use <../Modules/helix.scad>
use <../Modules/Cylinder/spool.scad>
use <../Modules/Shapes/lobes.scad>
//-----------------------------------------------------------------------------
sizes     = [
    [ 52, 59 ], // bq
    [ 57, 64 ], // FeelsGood
    [ 57, 37 ], // SUNLU 0.5kg
    [ 73, 65 ], // 3DWarHorse
];
size      = sizes[2];
bDiameter = size[0];   // Diámetro de la base.
bHeight   = 15;        // Altura de la base.
bType     =  2;        // Tipo de base a generar llamando al módulo `base{bType}`);
height    = size[1];   // Grosor del carrete que se quiere sujetar.
count     =  4;        // Cantidad de palas a dibujar.
pitch     =  8;        // Paso de la rosca.
rod       =  9.0;      // Diámetro de la varilla central.
thickness =  3.6;      // Grosor de las paredes.
tolerance =  0.03;     // Tolerancia entre las roscas de la tuerca y el tornillo con relación al paso.
pellet    =  0; //4.5; // Diámetro de la munición o balín a usar como rodamiento si se quieren apilar los carretes.
bearing   = false;     // `false` o medidas del rodamiento ([ diámetro externo, grosor ]).
                       // El diámetro interno se especifica en la variable `rod`.
//-----------------------------------------------------------------------------
epsilon = $preview ? 0.01 : 0;
radius  = max(rod / 2, bearing ? bearing[0] / 2 : 0) + thickness; // Radio de las paredes alrededor del agujero central.
$fn     = $preview ? 80 : 160;
//-----------------------------------------------------------------------------

/**
 * Dibuja la base según el valor de la variable `type`.
 */
module base(type = bType)
{
    difference()
    {
        if (type == 0)
        {
            base0();
        }
        else if (type == 1)
        {
            base1();
        }
        else if (type == 2)
        {
            base2();
        }
        else if (type == 3)
        {
            base3();
        }
        else if (type == 4)
        {
            base4();
        }
        else
        {
            cylinder(r = radius + 3 * thickness, h = thickness / 2);
        }
        if (pellet > 0)
        {
            translate([ 0, 0, - 0.6 ])
            {
                rotate_extrude(convexity = 10)
                {
                    translate([ bDiameter / 2, 0 ])
                    {
                        circle(d = pellet);
                    }
                }
            }
        }
    }
}

/**
 * Base usada para pruebas del tornillo y rosca.
 */
module base0()
{
    cylinder(r = radius + thickness, h = bHeight);
    cylinder(r = radius + 3 * thickness, h = thickness / 2);
}

/**
 * Dibuja la base tanto de la tuerca como del tornillo usando segmentos
 * dando como resultado una forma cónica.
 */
module base1()
{
    /**
     * Figura 2D usada como base rotándola.
     */
    module b(r, l)
    {
        translate([ radius, - r ])
        {
            square([ l, 2 * r ]);
            translate([ l, r ])
            {
                circle(r = r);
            }
        }
    }
    //--------------------------------------------------------
    cylinder(r = radius + thickness, h = bHeight);
    difference()
    {
        cylinder(d = bDiameter, h = thickness);
        translate([ 0, 0, - epsilon / 2 ])
        {
            cylinder(d = bDiameter - 2 * thickness, h = thickness + epsilon);
        }
    }
    for (_a = [ 0 : 360 / count : 359 ])
    {
        rotate([ 0, 0, _a ])
        {
            render()
            {
                _l = bDiameter / 2 - radius - thickness / 2;
                // Parte inferior recta.
                linear_extrude(thickness)
                {
                    b(thickness / 2, _l);
                }
                translate([ 0, 0, thickness - epsilon ])
                {
                    hull()
                    {
                        _e = 0.001;
                        // Parte inferior donde empieza la conicidad.
                        linear_extrude(_e)
                        {
                            b(thickness / 2, _l);
                        }
                        // Parte superior donde termina la conicidad.
                        translate([ 0, 0, bHeight - thickness - 2 * _e ])
                        {
                            linear_extrude(_e)
                            {
                                b(thickness / 2, thickness / 2);
                            }
                        }
                    }
                }
            }
        }
    }
}

/**
 * Dibuja la base tanto de la tuerca como del tornillo con forma cilíndrica para usarse
 * con carretes de diámetro fijo.
 */
module base2()
{
    spool(bDiameter, bHeight, thickness, 0, [  for (_a = [ 0 : 360 / count : 359 ]) _a ]);
    cylinder(r = radius + thickness, h = bHeight);
    difference()
    {
        cylinder(d = bDiameter + 2 * thickness, h = thickness);
        translate([ 0, 0, - epsilon / 2 ])
        {
            cylinder(d = bDiameter, h = thickness + epsilon);
        }
    }
}

/**
 * Dibuja una base cónica pero eliminando unas porciones.
 */
module base3()
{
    difference()
    {
        cylinder(r2 = radius + thickness, d1 = bDiameter, h = bHeight);
        for (_a = [ 0 : 360 / count : 359 ])
        {
            rotate([ 0, 0, _a ])
            {
                translate([ bDiameter / 2 + thickness, 0, - epsilon ])
                {
                    scale([ 1, 1.33, 1 ])
                    {
                        cylinder(r = bDiameter / count, h = bHeight + 2 * epsilon);
                    }
                }
            }
        }
    }
}

/**
 * Dibuja una base lobulada.
 */
module base4(count = count, ratio = 0.28)
{
    _scale = 2 * (radius + thickness) / bDiameter;
    linear_extrude(bHeight, convexity = 10, scale = 2 * _scale)
    {
        lobes(bDiameter, count, ratio);
    }
}

/**
 * Dibuja un sólido para dejar el espacio para el rodamiento.
 */
module bearing()
{
    _d = bearing[0];
    _h = bearing[1];
    cylinder(d = _d, h = _h);
    translate([ 0, 0, _h ])
    {
        cylinder(d1 = _d, d2 = 0, h = _d / 2);
    }
}

/**
 * Dibuja la tuerca.
 */
module nut()
{
    _h = 2 * thickness;
    difference()
    {
        if ($children)
        {
            children();
        }
        else
        {
            base();
        }
        translate([ 0, 0, - pitch ])
        {
            thread(bHeight + 2 * pitch, - tolerance);
        }
        // Avellanado de los extremos.
        translate([ 0, 0, bHeight / 2 ])
        {
            _r = radius + thickness / 2;
            for (_m = (bearing ? [ 0 ] : [ 0, 1 ]))
            {
                mirror([ 0, 0, _m ])
                {
                    translate([ 0, 0, bHeight / 2 - thickness + epsilon ])
                    {
                        cylinder(r1 = _r - thickness, r2 = _r, h = thickness);
                    }
                }
            }
        }
    }
}

/**
 * Dibuja el tornillo.
 */
module screw()
{
    _h = height + 2 * thickness; // Sumamos el grosor de la base y la tapa que sirve de tope.
    difference()
    {
        union()
        {
            if ($children)
            {
                children();
            }
            else
            {
                base();
            }
            thread(_h, tolerance);
        }
        translate([ 0, 0, - 0.01 ])
        {
            cylinder(d = rod, h = 2 * _h);
        }
        if (bearing)
        {
            translate([ 0, 0, - epsilon ])
            {
                bearing();
            }
            translate([ 0, 0, _h + epsilon ])
            {
                rotate([ 180, 0, 0 ])
                {
                    bearing();
                }
            }
        }
    }
}

module thread(height = height, tolerance = 0)
{
    helix(
       length           = height + epsilon,
       pitch            = pitch,
       pitchRadius      = radius,
       shaft            = 0.1,
       tolerance        = tolerance,
       threadAngle      = 45,
       threadPitchRatio = 0.25
    );
}

//------------------------------------------------------------
echo(str("SpoolHolder-", bDiameter, "x", height, "x", thickness, "-b", bType, "x", count, ".stl"));
screw();
translate([ bDiameter + 3 * thickness, 0, 0 ])
{
    nut();
}
