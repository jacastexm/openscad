/**
 * Dibuja un marco redondeado..
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Box/frame.scad
 * @license CC-BY-NC-4.0
 *
 * @param {Float} width  Ancho del marco (eje X).
 * @param {Float} height Alto del marco (eje Y).
 * @param {Float} radius Radio de los segmentos del marco.
 */
module boxFrame(width, height, radius)
{
    _d = 2 * radius;
    for (_x = [ -1, 1 ])
    {
        translate([ _x * (width - _d) / 2, 0, 0 ])
        {
            rotate([ 90, 0, 0 ])
            {
                cylinder(r = radius, h = height - _d, center = true);
            }
        }
        for (_y = [ -1, 1 ])
        {
            translate([ _x * (width - _d) / 2, _y * (height - _d) / 2, 0 ])
            {
                sphere(r = radius);
            }
        }
    }
    for (_y = [ -1, 1 ])
    {
        translate([ 0, _y * (height - _d) / 2, 0 ])
        {
            rotate([ 0, 90, 0 ])
            {
                cylinder(r = radius, h = width - _d, center = true);
            }
        }
    }
}
