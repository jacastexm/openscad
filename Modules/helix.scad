/**
 * Módulo para generar espirales 3D y tornillos.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/helix.scad
 * @license CC-BY-NC-4.0
 * @see     https://www.thingiverse.com/thing:4404727
 */
//-----------------------------------------------------------------------------
use <../Functions/Math/vectors.scad>
//-----------------------------------------------------------------------------
// Funciones
//-----------------------------------------------------------------------------

/**
 * Construye los puntos para dibujar las caras del poliedro.
 *
 * @param {Array} Listado de valores a usar.
 */
function helixFacesPoints(data) = let(
    _top      = data[0],
    _bottom   = data[1],
    _position = data[2],
    _ratio    = data[3],
    _depth    = data[4],
    _angle    = data[5],
    _shaft    = data[6],
    _vectb    = uvec(_top - _bottom) * _depth / 2 * tan(_angle),
    _vects    = uvec(_top - _shaft) * _depth,
    _bc1      = centroid(_top, _bottom, _position + _ratio / 2),
    _bc2      = centroid(_top, _bottom, _position - _ratio / 2)
) [
    _bc1 + _vectb,
    _bc1 - _vectb + _vects,
    centroid(_top, _bottom, _position),
    _bc2 + _vectb + _vects,
    _bc2 - _vectb
];

/**
 * Convierte a sistema cilíndrico los valores de la ubicación del punto del poliedro.
 */
function helixToCylindrical(radius, value, pitch) = toCylindrical(radius, value, helixZ(pitch, value));

/**
 * Devuelve la coordenada Z para el paso y el valor especificado.
 *
 * @param {Float} pitch Valor del paso.
 * @param {Float} value Valor radial actual.
 */
function helixZ(pitch, value) = pitch * value / 360;

//-----------------------------------------------------------------------------
// Módulos
//-----------------------------------------------------------------------------

/**
 * Dibuja una rosca usando un filete trapezoidal.
 *
 * @param {Float}   length           Longitud axial de la rosca.
 * @param {Float}   pitch            Paso o distancia axial entre filetes.
 * @param {Float}   pitchRadius      Distancia axial a la mitad del filete.
 * @param {Float}   threadPitchRatio Relación entre el alto del perfil y el paso.
 * @param {Float}   profileRatio     Relación entre las longitudes de la parte elevada del perfil y el paso.
 * @param {Float}   threadAngle      Ángulo entre las dos caras de cada filete. Para ACME es 29.
 * @param {Boolean} ccw              Sentido de giro (`true` para giro a la izquierda).
 * @param {Float}   tolerance        Holgura de la rosca normalizada con respecto al paso.
 * @param {Float}   startRatio       Relación a usar para el inicio del primer filete.
 * @param {Float}   shaft            Configura el dibujo del eje y su avellanado (el tamaño del avellanado es relativo al paso):
 *                                   - = 0: No se dibuja el eje.
 *                                   - < 0: Se dibuja el eje sobre el que gira la rosca sin avellanar.
 *                                   - > 0: Se dibuja el eje y se avellanan los extremosTamaño del avellanado con respecto al paso.
 *
 * Los valores por defecto son los usados por el sistema métrico.
 */
module helix(length = 10, pitch = 3, pitchRadius = 5, threadPitchRatio = 0.5, profileRatio = 0.5, threadAngle = 30, ccw = false, tolerance = 0.1, startRatio = 1/3, shaft = 0.1)
{
    $fn            = $fn ? $fn : 60;
    _pitchRadius   = pitchRadius - tolerance * pitch;
    _innerRadius   = _pitchRadius - (0.5 + tolerance) * pitch * threadPitchRatio;
    // Valores para el avellanado.
    _x0            = _innerRadius;
    _r1            = _pitchRadius - startRatio * pitch;
    _h             = shaft > 0 ? abs(_x0 - _r1) : 0;
    // Valores calculados.
    _length        = length - 2 * _h;
    _profileRatio  = profileRatio * (1 - tolerance);
    _threadHeight  = pitch * threadPitchRatio * (1 + tolerance);
    _totalTurns    = _length / pitch;
    _steps         = $fn * _totalTurns;
    _position      = (_profileRatio + threadPitchRatio * (1 + tolerance) * tan(threadAngle)) / 2;
    _degreesByStep = 360 * _totalTurns / _steps;
    _div           = $fn * startRatio;
    // Puntos de todos los poliedros a dibujar.
    _points = [
        for (_s = [ 0 : _steps ]) helixFacesPoints(
             concat(
                [
                    helixToCylindrical(_innerRadius, _degreesByStep * (_s + $fn), pitch),
                    helixToCylindrical(_innerRadius, _degreesByStep * _s, pitch),
                    _position
                ],
                [ _profileRatio, _threadHeight ] * min(min(_s, _steps - _s) / _div, 1),
                threadAngle,
                [
                    [ 0, 0, helixZ(pitch, _degreesByStep * (_s + $fn)) ]
                ]
            )
        )
    ];
    // Calculamos los valores para el primer punto para obtener la elevación inicial y el radio del avellanado
    translate([ 0, 0, - _points[0][0][2] + _h ])
    {
        mirror([ 0, ccw ? 1 : 0, 0 ])
        {
            for (_s = [ 0 : _steps - 1 ])
            {
                helixPolyhedron(_points[_s], _points[_s + 1]);
            }
        }
    }
    if (shaft)
    {
        translate([ 0, 0, length / 2 ])
        {
            cylinder(r = _innerRadius + 0.01, h = _length + _h, center = true);
        }
        if (shaft > 0)
        {
            cylinder(h = _h / 2, r1 = _r1, r2 = _x0);
            translate([ 0, 0, length - _h / 2 ])
            {
                cylinder(h = _h / 2, r1 = _x0, r2 = _r1);
            }
        }
    }
}

/**
 * Dibuja el poliedro para el segmento de la rosca.
 *
 * @param {Array} from Datos obtenidos de la función `helixFacesPoints` para el punto inicial.
 * @param {Array} to   Datos obtenidos de la función `helixFacesPoints` para el punto final.
 */
module helixPolyhedron(from, to)
{
    polyhedron(
        points = concat(from, to),
        faces  = [
            [ 0, 5, 6 ], [ 6, 1, 0 ],                           // Plano superior
            [ 1, 6, 8 ], [ 8, 3, 1 ],                           // Plano frontal
            [ 3, 8, 9 ], [ 9, 4, 3 ],                           // Plano inferior
            [ 0, 1, 2 ], [ 2, 1, 3 ], [ 3, 4, 2 ],              // Plano lateral 1
            [ 5, 7, 6 ], [ 6, 7, 8 ], [ 8, 7, 9 ],              // Plano lateral 2
            [ 0, 2, 5 ], [ 5, 2, 7 ], [ 7, 2, 4 ], [ 4, 9, 7 ]  // Plano trasero
        ]
    );
}
