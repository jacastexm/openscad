/**
 * Repite un elemento y dibuja su etiqueta correspondiente.
 *
 * @param {String[]} labels Listado de etiquetas.
 * @param {Float}    item   Dimensión del elemento. Si es un array, se considera como las dimensiones de un rectángulo.
 *                          En caso contrario se asume como el radio de un círculo.
 * @param {Float}    height Altura del conjunto de elementos (eje Y).
 *                          Si no se especifica se calcula a partir de las dimensiones del elemento.
 * @param {Float}    width  Ancho de la superficie donde se distribuirán los elementos (eje X).
 * @param {Float}    offset Desplazamiento a usar para colocar la etiqueta si existe algún borde en el elemento (eje Z).
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Modules/Utils/repeat.scad
 * @license CC-BY-NC-4.0
 */
//----------------------------------------------------------
use <./flex.scad>
//----------------------------------------------------------
/**
 * Repite una figura una cantidad de veces en un ancho especificado.
 *
 * @param {Float}     width     Ancho de la repetición (eje X).
 * @param {String[]}  labels    Etiquetas a colocar debajo de cada elemento.
 * @param {Float}     thickness Grosor de las figuras.
 * @param {Float[]}   item      Configuración del elemento a dibujar.
 *                              Un solo valor indica el diámetro del círculo y una lista se toma como las dimensiones de un rectángulo.
 * @param {Float}     height    Alto de la superficie resultante.
 * @param {Float}     offset    Distancia a separar la etiquetas.
 * @param {Integer[]} skip      Índices de los elementos a omitir. Se puede usar para dejar espacios.
 * @param {Float}     fontSize  Tamaño de las fuentes de las etiquetas.
 */
module utilsRepeat(width, labels, thickness, item = [ 22, 29 ], height = 0, offset = 6, skip = [], fontSize = 5)
{
    _e  = $preview ? 0.001 : 0;
    _w  = is_list(item) ? item[0] : item;
    _h  = is_list(item) ? item[1] : item;
    _iz = - _h / 2 - offset; // Offset para las etiquetas
    difference()
    {
        cube([ width, thickness, max(height, _h) ], center = true);
        translate([ 0, - thickness - _e, fontSize ])
        {
            rotate([ 90, 0, 180 ])
            {
                linear_extrude(2 * thickness, convexity = 10)
                {
                    flexCenter(width - _w, len(labels), [ for (_label = labels) [ _label, _iz, fontSize ] ], skip = skip)
                    {
                        if (is_list(item))
                        {
                            square(item, center = true);
                        }
                        else
                        {
                            circle(d = item);
                        }
                    }
                }
            }
        }
    }
    //----------------------------------------------------------
    // Relleno interior para las letras
    //----------------------------------------------------------
    _ly = thickness / 2;
    translate([ 0, - _ly / 2 - _e, fontSize + _iz ])
    {
        cube([ width, _ly, len(labels) ], center = true);
    }
}
