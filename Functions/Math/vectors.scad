/**
 * Módulo que incluye algunas funciones para trabajar con vectores.
 *
 * @author  Joaquín Fernández
 * @url     https://gitlab.com/joaquinfq/openscad/blob/master/Functions/Math/vectors.scad
 * @license CC-BY-NC-4.0
 */

/**
 * Devuelve el centroide de dos vectores.
 *
 * @param {Float[]} vector1 Vector 1.
 * @param {Float[]} vector2 Vector 2.
 * @param {Float}   ratio   Relación entre los módulos de ambos vectores.
 *
 * @return {Float[]} Vector del centroide.
 */
function centroid(vector1, vector2, ratio) = vector1 * ratio + vector2 * (1 - ratio);

/**
 * Convierte a sistema cilíndrico los valores del sistema esférico especificados.
 *
 * @param {Float} radius Valor del radio.
 * @param {Float} angle  Valor del ángulo en grados.
 * @param {Float} z      Valor del eje Z.
 *
 * @return {Float[]} Vector del sistema cilíndrico.
 */
function toCylindrical(radius, angle, z) = [ radius * cos(angle), radius * sin(angle), z ];

/**
 * Devuelve el vector unitario del vector especificado.
 *
 * @param {Float[]} vector Vector de entrada.
 *
 * @return {Float[]} Vector del centroide.
 */
function uvec(vector) = vector / norm(vector);
